# StoreDetail


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**ViasAddress**](ViasAddress.md) |  | 
**merchant_account** | **str** | The merchant account to which the store belongs. | 
**merchant_category_code** | **str** | The merchant category code (MCC) that classifies the business of the account holder. | 
**store_reference** | **str** | Your unique identifier for the store, between 3 to 128 characters in length. This value will be shown as the store description in your Customer Area. | 
**full_phone_number** | **str** | The phone number of the store provided as a single string.  It will be handled as a landline phone.  Examples: \&quot;0031 6 11 22 33 44\&quot;, \&quot;+316/1122-3344\&quot;, \&quot;(0031) 611223344\&quot; | [optional] 
**phone_number** | [**ViasPhoneNumber**](ViasPhoneNumber.md) |  | [optional] 
**shopper_interaction** | **str** | The sales channel. Possible values: **Ecommerce**, **POS**. | [optional] 
**split_configuration_uuid** | **str** | The unique reference for the split configuration, returned when you configure splits in your Customer Area. When this is provided, the &#x60;virtualAccount&#x60; is also required. Adyen uses the configuration and the &#x60;virtualAccount&#x60; to split funds between accounts in your platform. | [optional] 
**status** | **str** | The status of the store. Possible values: **Pending**, **Active**, **Inactive**, **InactiveWithModifications**, **Closed**. | [optional] [readonly] 
**store** | **str** | Adyen-generated unique alphanumeric identifier (UUID) for the store, returned in the response when you create a store. Required when updating an existing store in an &#x60;/updateAccountHolder&#x60; request. | [optional] 
**store_name** | **str** | The name of the account holder&#39;s store, between  3 to 22 characters in length. This value will be shown in shopper statements. | [optional] 
**virtual_account** | **str** | The account holder&#39;s &#x60;accountCode&#x60; where the split amount will be sent. Required when you provide the &#x60;splitConfigurationUUID&#x60;. | [optional] 
**web_address** | **str** | URL of the ecommerce store. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


