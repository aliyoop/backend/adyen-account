# FieldType


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **str** | The full name of the property. | [optional] 
**field_name** | **str** | The type of the field. | [optional] 
**shareholder_code** | **str** | The code of the shareholder that the field belongs to. If empty, the field belongs to an account holder. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


