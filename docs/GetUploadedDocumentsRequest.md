# GetUploadedDocumentsRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_holder_code** | **str** | The code of the Account Holder for which to retrieve the documents. | 
**bank_account_uuid** | **str** | The code of the Bank Account for which to retrieve the documents. | [optional] 
**shareholder_code** | **str** | The code of the Shareholder for which to retrieve the documents. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


