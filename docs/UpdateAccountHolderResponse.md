# UpdateAccountHolderResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_holder_status** | [**AccountHolderStatus**](AccountHolderStatus.md) |  | 
**legal_entity** | **str** | The legal entity of the account holder. | 
**verification** | [**KYCVerificationResult2**](KYCVerificationResult2.md) |  | 
**account_holder_code** | **str** | The code of the account holder. | [optional] 
**account_holder_details** | [**AccountHolderDetails**](AccountHolderDetails.md) |  | [optional] 
**description** | **str** | The description of the account holder. | [optional] 
**invalid_fields** | [**[ErrorFieldType]**](ErrorFieldType.md) | in case the account holder has not been updated, contains account holder fields, that did not pass the validation. | [optional] 
**primary_currency** | **str** | The three-character [ISO currency code](https://docs.adyen.com/development-resources/currency-codes), with which the prospective account holder primarily deals. | [optional] 
**psp_reference** | **str** | The reference of a request. Can be used to uniquely identify the request. | [optional] 
**result_code** | **str** | The result code. | [optional] 
**verification_profile** | **str** | The identifier of the profile that applies to this entity. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


