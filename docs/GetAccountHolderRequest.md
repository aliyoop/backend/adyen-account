# GetAccountHolderRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_code** | **str** | The code of the account of which to retrieve the details. &gt; Required if no &#x60;accountHolderCode&#x60; is provided. | [optional] 
**account_holder_code** | **str** | The code of the account holder of which to retrieve the details. &gt; Required if no &#x60;accountCode&#x60; is provided. | [optional] 
**show_details** | **bool** | True if the request should return the account holder details | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


