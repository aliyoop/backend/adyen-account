# AccountPayoutState


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allow_payout** | **bool** | Indicates whether payouts are allowed. This field is the overarching payout status, and is the aggregate of multiple conditions (e.g., KYC status, disabled flag, etc). If this field is false, no payouts will be permitted for any of the account holder&#39;s accounts. If this field is true, payouts will be permitted for any of the account holder&#39;s accounts. | [optional] 
**disable_reason** | **str** | The reason why payouts (to all of the account holder&#39;s accounts) have been disabled (by the platform). If the &#x60;disabled&#x60; field is true, this field can be used to explain why. | [optional] 
**disabled** | **bool** | Indicates whether payouts have been disabled (by the platform) for all of the account holder&#39;s accounts. A platform may enable and disable this field at their discretion. If this field is true, &#x60;allowPayout&#x60; will be false and no payouts will be permitted for any of the account holder&#39;s accounts. If this field is false, &#x60;allowPayout&#x60; may or may not be enabled, depending on other factors. | [optional] 
**not_allowed_reason** | **str** | The reason why payouts (to all of the account holder&#39;s accounts) have been disabled (by Adyen). If payouts have been disabled by Adyen, this field will explain why. If this field is blank, payouts have not been disabled by Adyen. | [optional] 
**payout_limit** | [**Amount**](Amount.md) |  | [optional] 
**tier_number** | **int** | The payout tier that the account holder occupies. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


