# KYCVerificationResult2


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_holder** | [**KYCCheckResult2**](KYCCheckResult2.md) |  | [optional] 
**legal_arrangements** | [**[KYCLegalArrangementCheckResult]**](KYCLegalArrangementCheckResult.md) | The results of the checks on the legal arrangements. | [optional] 
**legal_arrangements_entities** | [**[KYCLegalArrangementEntityCheckResult]**](KYCLegalArrangementEntityCheckResult.md) | The results of the checks on the legal arrangement entities. | [optional] 
**payout_methods** | [**[KYCPayoutMethodCheckResult]**](KYCPayoutMethodCheckResult.md) | The results of the checks on the payout methods. | [optional] 
**shareholders** | [**[KYCShareholderCheckResult]**](KYCShareholderCheckResult.md) | The results of the checks on the shareholders. | [optional] 
**signatories** | [**[KYCSignatoryCheckResult]**](KYCSignatoryCheckResult.md) | The results of the checks on the signatories. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


