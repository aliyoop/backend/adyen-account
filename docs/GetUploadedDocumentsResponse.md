# GetUploadedDocumentsResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_details** | [**[DocumentDetail]**](DocumentDetail.md) | A list of the documents and their details. | [optional] 
**invalid_fields** | [**[ErrorFieldType]**](ErrorFieldType.md) | Contains field validation errors that would prevent requests from being processed. | [optional] 
**psp_reference** | **str** | The reference of a request. Can be used to uniquely identify the request. | [optional] 
**result_code** | **str** | The result code. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


