# KYCSignatoryCheckResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checks** | [**[KYCCheckStatusData]**](KYCCheckStatusData.md) | A list of the checks and their statuses. | [optional] 
**signatory_code** | **str** | The code of the signatory to which the check applies. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


