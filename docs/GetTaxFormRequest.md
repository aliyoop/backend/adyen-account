# GetTaxFormRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_holder_code** | **str** | The account holder code you provided when you created the account holder. | 
**form_type** | **str** | Type of the requested tax form. For example, 1099-K. | 
**year** | **int** | Applicable tax year in the YYYY format. | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


