# AccountEvent


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event** | **str** | The event. &gt;Permitted values: &#x60;InactivateAccount&#x60;, &#x60;RefundNotPaidOutTransfers&#x60;. For more information, refer to [Verification checks](https://docs.adyen.com/platforms/verification-checks). | 
**execution_date** | **datetime** | The date on which the event will take place. | 
**reason** | **str** | The reason why this event has been created. | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


