# LegalArrangementEntityDetail


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**legal_entity_type** | **str** | The legal entity type.  Possible values: **Business**, **Individual**, **NonProfit**, **PublicCompany**, or **Partnership**.  | 
**address** | [**ViasAddress**](ViasAddress.md) |  | [optional] 
**business_details** | [**BusinessDetails**](BusinessDetails.md) |  | [optional] 
**email** | **str** | The e-mail address of the entity. | [optional] 
**full_phone_number** | **str** | The phone number of the contact provided as a single string.  It will be handled as a landline phone. **Examples:** \&quot;0031 6 11 22 33 44\&quot;, \&quot;+316/1122-3344\&quot;, \&quot;(0031) 611223344\&quot; | [optional] 
**individual_details** | [**IndividualDetails**](IndividualDetails.md) |  | [optional] 
**legal_arrangement_entity_code** | **str** | Adyen-generated unique alphanumeric identifier (UUID) for the entry, returned in the response when you create a legal arrangement entity. | [optional] 
**legal_arrangement_entity_reference** | **str** | Your reference for the legal arrangement entity. | [optional] 
**legal_arrangement_member** | **str** | The role of the entity in the legal arrangement. The values depend on the &#x60;legalArragementType&#x60;.  Possible values:  - For &#x60;legalArragementType&#x60; **Trust**, you can use **Trustee**, **Settlor**, **Protector**, **Beneficiary**, or **Shareholder**.  - For &#x60;legalArragementType&#x60; **Partnership**, you can use **Partner** or **Shareholder**.  | [optional] 
**phone_number** | [**ViasPhoneNumber**](ViasPhoneNumber.md) |  | [optional] 
**web_address** | **str** | The URL of the website of the contact. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


