# CreateAccountResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_code** | **str** | The code of the new account. | 
**account_holder_code** | **str** | The code of the account holder. | 
**status** | **str** | The status of the account. &gt;Permitted values: &#x60;Active&#x60;. | 
**bank_account_uuid** | **str** | The bankAccountUUID of the bank account held by the account holder to couple the account with. Scheduled payouts in currencies matching the currency of this bank account will be sent to this bank account. Payouts in different currencies will be sent to a matching bank account of the account holder. | [optional] 
**description** | **str** | The description of the account. | [optional] 
**invalid_fields** | [**[ErrorFieldType]**](ErrorFieldType.md) | A list of fields that caused the &#x60;/createAccount&#x60; request to fail. | [optional] 
**metadata** | **{str: (str,)}** | A set of key and value pairs containing metadata. | [optional] 
**payout_method_code** | **str** | The payout method code held by the account holder to couple the account with. Scheduled card payouts will be sent using this payout method code. | [optional] 
**payout_schedule** | [**PayoutScheduleResponse**](PayoutScheduleResponse.md) |  | [optional] 
**payout_speed** | **str** | Speed with which payouts for this account are processed. Permitted values: &#x60;STANDARD&#x60;, &#x60;SAME_DAY&#x60;. | [optional] 
**psp_reference** | **str** | The reference of a request. Can be used to uniquely identify the request. | [optional] 
**result_code** | **str** | The result code. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


