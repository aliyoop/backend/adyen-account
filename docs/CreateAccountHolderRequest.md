# CreateAccountHolderRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_holder_code** | **str** | Your unique identifier for the prospective account holder. The length must be between three (3) and fifty (50) characters long. Only letters, digits, and hyphens (-) are allowed. | 
**account_holder_details** | [**AccountHolderDetails**](AccountHolderDetails.md) |  | 
**legal_entity** | **str** | The legal entity type of the account holder. This determines the information that should be provided in the request.  Possible values: **Business**, **Individual**, or **NonProfit**.  * If set to **Business** or **NonProfit**, then &#x60;accountHolderDetails.businessDetails&#x60; must be provided, with at least one entry in the &#x60;accountHolderDetails.businessDetails.shareholders&#x60; list.  * If set to **Individual**, then &#x60;accountHolderDetails.individualDetails&#x60; must be provided. | 
**create_default_account** | **bool** | If set to **true**, an account with the default options is automatically created for the account holder. By default, this field is set to **true**. | [optional] 
**description** | **str** | A description of the prospective account holder, maximum 256 characters. You can use alphanumeric characters (A-Z, a-z, 0-9), white spaces, and underscores &#x60;_&#x60;. | [optional] 
**primary_currency** | **str** | The three-character [ISO currency code](https://docs.adyen.com/development-resources/currency-codes), with which the prospective account holder primarily deals. | [optional] 
**processing_tier** | **int** | The starting [processing tier](https://docs.adyen.com/platforms/onboarding-and-verification/precheck-kyc-information) for the prospective account holder. | [optional] 
**verification_profile** | **str** | The identifier of the profile that applies to this entity. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


