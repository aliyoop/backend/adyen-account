# PerformVerificationRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_holder_code** | **str** | The code of the account holder to verify. | 
**account_state_type** | **str** | The state required for the account holder. &gt; Permitted values: &#x60;Processing&#x60;, &#x60;Payout&#x60;. | 
**tier** | **int** | The tier required for the account holder. | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


