# BusinessDetails


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**doing_business_as** | **str** | The registered name of the company (if it differs from the legal name of the company). | [optional] 
**legal_business_name** | **str** | The legal name of the company. | [optional] 
**registration_number** | **str** | The registration number of the company. | [optional] 
**shareholders** | [**[ShareholderContact]**](ShareholderContact.md) | Array containing information about individuals associated with the account holder either through ownership or control. For details about how you can identify them, refer to [Identity check](https://docs.adyen.com/platforms/verification-checks/identity-check). | [optional] 
**signatories** | [**[SignatoryContact]**](SignatoryContact.md) | Signatories associated with the company. Each array entry should represent one signatory. | [optional] 
**stock_exchange** | **str** | Market Identifier Code (MIC). | [optional] 
**stock_number** | **str** | International Securities Identification Number (ISIN). | [optional] 
**stock_ticker** | **str** | Stock Ticker symbol. | [optional] 
**tax_id** | **str** | The tax ID of the company. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


