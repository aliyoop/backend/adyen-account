# AccountHolderStatus


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | The status of the account holder. &gt;Permitted values: &#x60;Active&#x60;, &#x60;Inactive&#x60;, &#x60;Suspended&#x60;, &#x60;Closed&#x60;. | 
**events** | [**[AccountEvent]**](AccountEvent.md) | A list of events scheduled for the account holder. | [optional] 
**payout_state** | [**AccountPayoutState**](AccountPayoutState.md) |  | [optional] 
**processing_state** | [**AccountProcessingState**](AccountProcessingState.md) |  | [optional] 
**status_reason** | **str** | The reason why the status was assigned to the account holder. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


