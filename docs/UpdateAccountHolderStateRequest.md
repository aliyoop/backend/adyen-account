# UpdateAccountHolderStateRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_holder_code** | **str** | The code of the Account Holder on which to update the state. | 
**disable** | **bool** | If true, disable the requested state.  If false, enable the requested state. | 
**state_type** | **str** | The state to be updated. &gt;Permitted values are: &#x60;Processing&#x60;, &#x60;Payout&#x60; | 
**reason** | **str** | The reason that the state is being updated. &gt;Required if the state is being disabled. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


