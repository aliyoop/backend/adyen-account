# GetAccountHolderResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_holder_code** | **str** | The code of the account holder. | 
**account_holder_details** | [**AccountHolderDetails**](AccountHolderDetails.md) |  | 
**account_holder_status** | [**AccountHolderStatus**](AccountHolderStatus.md) |  | 
**legal_entity** | **str** | The legal entity of the account holder. | 
**verification** | [**KYCVerificationResult2**](KYCVerificationResult2.md) |  | 
**accounts** | [**[Account]**](Account.md) | A list of the accounts under the account holder. | [optional] 
**description** | **str** | The description of the account holder. | [optional] 
**invalid_fields** | [**[ErrorFieldType]**](ErrorFieldType.md) | Contains field validation errors that would prevent requests from being processed. | [optional] 
**primary_currency** | **str** | The three-character [ISO currency code](https://docs.adyen.com/development-resources/currency-codes), with which the prospective account holder primarily deals. | [optional] 
**psp_reference** | **str** | The reference of a request. Can be used to uniquely identify the request. | [optional] 
**result_code** | **str** | The result code. | [optional] 
**system_up_to_date_time** | **datetime** | The time that shows how up to date is the information in the response. | [optional] 
**verification_profile** | **str** | The identifier of the profile that applies to this entity. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


