# adyen_account.DefaultApi

All URIs are relative to *https://cal-test.adyen.com/cal/services/Account/v6*

Method | HTTP request | Description
------------- | ------------- | -------------
[**post_check_account_holder**](DefaultApi.md#post_check_account_holder) | **POST** /checkAccountHolder | Trigger verification.
[**post_close_account**](DefaultApi.md#post_close_account) | **POST** /closeAccount | Close an account.
[**post_close_account_holder**](DefaultApi.md#post_close_account_holder) | **POST** /closeAccountHolder | Close an account holder.
[**post_close_stores**](DefaultApi.md#post_close_stores) | **POST** /closeStores | Close stores.
[**post_create_account**](DefaultApi.md#post_create_account) | **POST** /createAccount | Create a new account.
[**post_create_account_holder**](DefaultApi.md#post_create_account_holder) | **POST** /createAccountHolder | Create a new account holder.
[**post_delete_bank_accounts**](DefaultApi.md#post_delete_bank_accounts) | **POST** /deleteBankAccounts | Delete bank accounts.
[**post_delete_payout_methods**](DefaultApi.md#post_delete_payout_methods) | **POST** /deletePayoutMethods | Delete payout methods.
[**post_delete_shareholders**](DefaultApi.md#post_delete_shareholders) | **POST** /deleteShareholders | Delete shareholders.
[**post_delete_signatories**](DefaultApi.md#post_delete_signatories) | **POST** /deleteSignatories | Delete signatories.
[**post_get_account_holder**](DefaultApi.md#post_get_account_holder) | **POST** /getAccountHolder | Get an account holder.
[**post_get_tax_form**](DefaultApi.md#post_get_tax_form) | **POST** /getTaxForm | Get a tax form.
[**post_get_uploaded_documents**](DefaultApi.md#post_get_uploaded_documents) | **POST** /getUploadedDocuments | Get documents.
[**post_suspend_account_holder**](DefaultApi.md#post_suspend_account_holder) | **POST** /suspendAccountHolder | Suspend an account holder.
[**post_un_suspend_account_holder**](DefaultApi.md#post_un_suspend_account_holder) | **POST** /unSuspendAccountHolder | Unsuspend an account holder.
[**post_update_account**](DefaultApi.md#post_update_account) | **POST** /updateAccount | Update an account.
[**post_update_account_holder**](DefaultApi.md#post_update_account_holder) | **POST** /updateAccountHolder | Update an account holder.
[**post_update_account_holder_state**](DefaultApi.md#post_update_account_holder_state) | **POST** /updateAccountHolderState | Update payout or processing state.
[**post_upload_document**](DefaultApi.md#post_upload_document) | **POST** /uploadDocument | Upload a document.


# **post_check_account_holder**
> GenericResponse post_check_account_holder()

Trigger verification.

Triggers the KYC verification for an account holder even if the checks are not yet required for the volume that they currently process.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.generic_response import GenericResponse
from adyen_account.model.perform_verification_request import PerformVerificationRequest
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    perform_verification_request = PerformVerificationRequest(None) # PerformVerificationRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Trigger verification.
        api_response = api_instance.post_check_account_holder(perform_verification_request=perform_verification_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_check_account_holder: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **perform_verification_request** | [**PerformVerificationRequest**](PerformVerificationRequest.md)|  | [optional]

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_close_account**
> CloseAccountResponse post_close_account()

Close an account.

Closes an account. If an account is closed, you cannot process transactions, pay out its funds, or reopen it. If payments are made to a closed account, the payments will be directed to your liable account.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.close_account_request import CloseAccountRequest
from adyen_account.model.close_account_response import CloseAccountResponse
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    close_account_request = CloseAccountRequest(None) # CloseAccountRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Close an account.
        api_response = api_instance.post_close_account(close_account_request=close_account_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_close_account: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **close_account_request** | [**CloseAccountRequest**](CloseAccountRequest.md)|  | [optional]

### Return type

[**CloseAccountResponse**](CloseAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_close_account_holder**
> CloseAccountHolderResponse post_close_account_holder()

Close an account holder.

Changes the [status of an account holder](https://docs.adyen.com/platforms/account-holders-and-accounts#account-holder-statuses) to **Closed**. This state is final. If an account holder is closed, you can't process transactions, pay out funds, or reopen it. If payments are made to an account of an account holder with a **Closed** status,the payments will be directed to your liable account.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.close_account_holder_request import CloseAccountHolderRequest
from adyen_account.model.close_account_holder_response import CloseAccountHolderResponse
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    close_account_holder_request = CloseAccountHolderRequest(None) # CloseAccountHolderRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Close an account holder.
        api_response = api_instance.post_close_account_holder(close_account_holder_request=close_account_holder_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_close_account_holder: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **close_account_holder_request** | [**CloseAccountHolderRequest**](CloseAccountHolderRequest.md)|  | [optional]

### Return type

[**CloseAccountHolderResponse**](CloseAccountHolderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_close_stores**
> GenericResponse post_close_stores()

Close stores.

Close one or more stores of the account holder.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.generic_response import GenericResponse
from adyen_account.model.close_stores_request import CloseStoresRequest
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    close_stores_request = CloseStoresRequest(None) # CloseStoresRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Close stores.
        api_response = api_instance.post_close_stores(close_stores_request=close_stores_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_close_stores: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **close_stores_request** | [**CloseStoresRequest**](CloseStoresRequest.md)|  | [optional]

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_create_account**
> CreateAccountResponse post_create_account()

Create a new account.

Creates an account under an account holder. An account holder can have [multiple accounts](https://docs.adyen.com/platforms/account-holders-and-accounts#create-additional-accounts).

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.create_account_request import CreateAccountRequest
from adyen_account.model.create_account_response import CreateAccountResponse
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    create_account_request = CreateAccountRequest(None) # CreateAccountRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a new account.
        api_response = api_instance.post_create_account(create_account_request=create_account_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_create_account: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_account_request** | [**CreateAccountRequest**](CreateAccountRequest.md)|  | [optional]

### Return type

[**CreateAccountResponse**](CreateAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_create_account_holder**
> CreateAccountHolderResponse post_create_account_holder()

Create a new account holder.

Creates an account holder, which [represents the sub-merchant's entity](https://docs.adyen.com/platforms/account-structure#your-platform) in your platform. The details that you need to provide in the request depend on the sub-merchant's legal entity type. For more information, refer to [Account holder and accounts](https://docs.adyen.com/platforms/account-holders-and-accounts#legal-entity-types).

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.create_account_holder_request import CreateAccountHolderRequest
from adyen_account.model.create_account_holder_response import CreateAccountHolderResponse
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    create_account_holder_request = CreateAccountHolderRequest(None) # CreateAccountHolderRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a new account holder.
        api_response = api_instance.post_create_account_holder(create_account_holder_request=create_account_holder_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_create_account_holder: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_account_holder_request** | [**CreateAccountHolderRequest**](CreateAccountHolderRequest.md)|  | [optional]

### Return type

[**CreateAccountHolderResponse**](CreateAccountHolderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_delete_bank_accounts**
> GenericResponse post_delete_bank_accounts()

Delete bank accounts.

Deletes one or more bank accounts of an account holder. 

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.generic_response import GenericResponse
from adyen_account.model.delete_bank_account_request import DeleteBankAccountRequest
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    delete_bank_account_request = DeleteBankAccountRequest(None) # DeleteBankAccountRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Delete bank accounts.
        api_response = api_instance.post_delete_bank_accounts(delete_bank_account_request=delete_bank_account_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_delete_bank_accounts: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_bank_account_request** | [**DeleteBankAccountRequest**](DeleteBankAccountRequest.md)|  | [optional]

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_delete_payout_methods**
> GenericResponse post_delete_payout_methods()

Delete payout methods.

Deletes one or more payout methods of an account holder.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.generic_response import GenericResponse
from adyen_account.model.delete_payout_method_request import DeletePayoutMethodRequest
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    delete_payout_method_request = DeletePayoutMethodRequest(None) # DeletePayoutMethodRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Delete payout methods.
        api_response = api_instance.post_delete_payout_methods(delete_payout_method_request=delete_payout_method_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_delete_payout_methods: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_payout_method_request** | [**DeletePayoutMethodRequest**](DeletePayoutMethodRequest.md)|  | [optional]

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_delete_shareholders**
> GenericResponse post_delete_shareholders()

Delete shareholders.

Deletes one or more shareholders from an account holder.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.generic_response import GenericResponse
from adyen_account.model.delete_shareholder_request import DeleteShareholderRequest
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    delete_shareholder_request = DeleteShareholderRequest(None) # DeleteShareholderRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Delete shareholders.
        api_response = api_instance.post_delete_shareholders(delete_shareholder_request=delete_shareholder_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_delete_shareholders: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_shareholder_request** | [**DeleteShareholderRequest**](DeleteShareholderRequest.md)|  | [optional]

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_delete_signatories**
> GenericResponse post_delete_signatories()

Delete signatories.

Deletes one or more signatories from an account holder.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.generic_response import GenericResponse
from adyen_account.model.delete_signatories_request import DeleteSignatoriesRequest
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    delete_signatories_request = DeleteSignatoriesRequest(None) # DeleteSignatoriesRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Delete signatories.
        api_response = api_instance.post_delete_signatories(delete_signatories_request=delete_signatories_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_delete_signatories: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_signatories_request** | [**DeleteSignatoriesRequest**](DeleteSignatoriesRequest.md)|  | [optional]

### Return type

[**GenericResponse**](GenericResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_get_account_holder**
> GetAccountHolderResponse post_get_account_holder()

Get an account holder.

Retrieves the details of an account holder.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.get_account_holder_response import GetAccountHolderResponse
from adyen_account.model.get_account_holder_request import GetAccountHolderRequest
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    get_account_holder_request = GetAccountHolderRequest(None) # GetAccountHolderRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get an account holder.
        api_response = api_instance.post_get_account_holder(get_account_holder_request=get_account_holder_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_get_account_holder: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **get_account_holder_request** | [**GetAccountHolderRequest**](GetAccountHolderRequest.md)|  | [optional]

### Return type

[**GetAccountHolderResponse**](GetAccountHolderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_get_tax_form**
> GetTaxFormResponse post_get_tax_form()

Get a tax form.

Generates a tax form for account holders operating in the US. For more information, refer to [Providing tax forms](https://docs.adyen.com/platforms/tax-forms).

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.get_tax_form_request import GetTaxFormRequest
from adyen_account.model.get_tax_form_response import GetTaxFormResponse
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    get_tax_form_request = GetTaxFormRequest(None) # GetTaxFormRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get a tax form.
        api_response = api_instance.post_get_tax_form(get_tax_form_request=get_tax_form_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_get_tax_form: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **get_tax_form_request** | [**GetTaxFormRequest**](GetTaxFormRequest.md)|  | [optional]

### Return type

[**GetTaxFormResponse**](GetTaxFormResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_get_uploaded_documents**
> GetUploadedDocumentsResponse post_get_uploaded_documents()

Get documents.

Retrieves documents that were previously uploaded for an account holder. Adyen uses the documents in the [KYC verification checks](https://docs.adyen.com/platforms/verification-checks). 

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.get_uploaded_documents_request import GetUploadedDocumentsRequest
from adyen_account.model.service_error import ServiceError
from adyen_account.model.get_uploaded_documents_response import GetUploadedDocumentsResponse
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    get_uploaded_documents_request = GetUploadedDocumentsRequest(None) # GetUploadedDocumentsRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get documents.
        api_response = api_instance.post_get_uploaded_documents(get_uploaded_documents_request=get_uploaded_documents_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_get_uploaded_documents: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **get_uploaded_documents_request** | [**GetUploadedDocumentsRequest**](GetUploadedDocumentsRequest.md)|  | [optional]

### Return type

[**GetUploadedDocumentsResponse**](GetUploadedDocumentsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_suspend_account_holder**
> SuspendAccountHolderResponse post_suspend_account_holder()

Suspend an account holder.

Changes the [status of an account holder](https://docs.adyen.com/platforms/account-holders-and-accounts#account-holder-statuses) to **Suspended**.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.suspend_account_holder_response import SuspendAccountHolderResponse
from adyen_account.model.service_error import ServiceError
from adyen_account.model.suspend_account_holder_request import SuspendAccountHolderRequest
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    suspend_account_holder_request = SuspendAccountHolderRequest(None) # SuspendAccountHolderRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Suspend an account holder.
        api_response = api_instance.post_suspend_account_holder(suspend_account_holder_request=suspend_account_holder_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_suspend_account_holder: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suspend_account_holder_request** | [**SuspendAccountHolderRequest**](SuspendAccountHolderRequest.md)|  | [optional]

### Return type

[**SuspendAccountHolderResponse**](SuspendAccountHolderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_un_suspend_account_holder**
> UnSuspendAccountHolderResponse post_un_suspend_account_holder()

Unsuspend an account holder.

Changes the [status of an account holder](https://docs.adyen.com/platforms/account-holders-and-accounts#account-holder-statuses) from **Suspended** to **Inactive**. Account holders can have a **Suspended** status if you suspend them through the [`/suspendAccountHolder`](https://docs.adyen.com/api-explorer/#/Account/v5/post/suspendAccountHolder) endpoint or if a KYC deadline expires.  You can only unsuspend account holders if they _do not_ have verification checks with a **FAILED** [`status`](https://docs.adyen.com/api-explorer/#/Account/latest/post/getAccountHolder__resParam_verification-accountHolder-checks-status).

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.un_suspend_account_holder_request import UnSuspendAccountHolderRequest
from adyen_account.model.un_suspend_account_holder_response import UnSuspendAccountHolderResponse
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    un_suspend_account_holder_request = UnSuspendAccountHolderRequest(None) # UnSuspendAccountHolderRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Unsuspend an account holder.
        api_response = api_instance.post_un_suspend_account_holder(un_suspend_account_holder_request=un_suspend_account_holder_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_un_suspend_account_holder: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **un_suspend_account_holder_request** | [**UnSuspendAccountHolderRequest**](UnSuspendAccountHolderRequest.md)|  | [optional]

### Return type

[**UnSuspendAccountHolderResponse**](UnSuspendAccountHolderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_update_account**
> UpdateAccountResponse post_update_account()

Update an account.

Updates the description or payout schedule of an account.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.update_account_request import UpdateAccountRequest
from adyen_account.model.update_account_response import UpdateAccountResponse
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    update_account_request = UpdateAccountRequest(None) # UpdateAccountRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update an account.
        api_response = api_instance.post_update_account(update_account_request=update_account_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_update_account: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **update_account_request** | [**UpdateAccountRequest**](UpdateAccountRequest.md)|  | [optional]

### Return type

[**UpdateAccountResponse**](UpdateAccountResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_update_account_holder**
> UpdateAccountHolderResponse post_update_account_holder()

Update an account holder.

Updates the `accountHolderDetails` and `processingTier` of an account holder, and adds bank accounts and shareholders.  When updating `accountHolderDetails`, parameters that are not included in the request are left unchanged except for the objects below.  * `metadata`: Updating the metadata replaces the entire object. This means that to update an existing key-value pair, you must provide the changes along with other existing key-value pairs.  When updating any field in the following objects, you must submit all the fields required for validation.   * `address`  * `fullPhoneNumber`  * `bankAccountDetails.BankAccountDetail`  * `businessDetails.shareholders.ShareholderContact`   For example, to update the `address.postalCode`, you must also submit the `address.country`, `.city`, `.street`, `.postalCode`, and possibly `.stateOrProvince` so that the address can be validated.  To add a bank account or shareholder, provide the bank account or shareholder details without a `bankAccountUUID` or a `shareholderCode`.  

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.update_account_holder_request import UpdateAccountHolderRequest
from adyen_account.model.update_account_holder_response import UpdateAccountHolderResponse
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    update_account_holder_request = UpdateAccountHolderRequest(None) # UpdateAccountHolderRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update an account holder.
        api_response = api_instance.post_update_account_holder(update_account_holder_request=update_account_holder_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_update_account_holder: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **update_account_holder_request** | [**UpdateAccountHolderRequest**](UpdateAccountHolderRequest.md)|  | [optional]

### Return type

[**UpdateAccountHolderResponse**](UpdateAccountHolderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_update_account_holder_state**
> GetAccountHolderStatusResponse post_update_account_holder_state()

Update payout or processing state.

Disables or enables the processing or payout state of an account holder.

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.update_account_holder_state_request import UpdateAccountHolderStateRequest
from adyen_account.model.service_error import ServiceError
from adyen_account.model.get_account_holder_status_response import GetAccountHolderStatusResponse
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    update_account_holder_state_request = UpdateAccountHolderStateRequest(None) # UpdateAccountHolderStateRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update payout or processing state.
        api_response = api_instance.post_update_account_holder_state(update_account_holder_state_request=update_account_holder_state_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_update_account_holder_state: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **update_account_holder_state_request** | [**UpdateAccountHolderStateRequest**](UpdateAccountHolderStateRequest.md)|  | [optional]

### Return type

[**GetAccountHolderStatusResponse**](GetAccountHolderStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_upload_document**
> UpdateAccountHolderResponse post_upload_document()

Upload a document.

Uploads a document for an account holder. Adyen uses the documents in the [KYC verification checks](https://docs.adyen.com/platforms/verification-checks).

### Example


```python
import time
import adyen_account
from adyen_account.api import default_api
from adyen_account.model.service_error import ServiceError
from adyen_account.model.update_account_holder_response import UpdateAccountHolderResponse
from adyen_account.model.upload_document_request import UploadDocumentRequest
from pprint import pprint
# Defining the host is optional and defaults to https://cal-test.adyen.com/cal/services/Account/v6
# See configuration.py for a list of all supported configuration parameters.
configuration = adyen_account.Configuration(
    host = "https://cal-test.adyen.com/cal/services/Account/v6"
)


# Enter a context with an instance of the API client
with adyen_account.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    upload_document_request = UploadDocumentRequest(None) # UploadDocumentRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Upload a document.
        api_response = api_instance.post_upload_document(upload_document_request=upload_document_request)
        pprint(api_response)
    except adyen_account.ApiException as e:
        print("Exception when calling DefaultApi->post_upload_document: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **upload_document_request** | [**UploadDocumentRequest**](UploadDocumentRequest.md)|  | [optional]

### Return type

[**UpdateAccountHolderResponse**](UpdateAccountHolderResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK - the request has succeeded. |  -  |
**202** | Accepted - the request has been accepted for processing, but the processing has not been completed. |  -  |
**400** | Bad Request - a problem reading or understanding the request. |  -  |
**401** | Unauthorized - authentication required. |  -  |
**403** | Forbidden - insufficient permissions to process the request. |  -  |
**422** | Unprocessable Entity - a request validation error. |  -  |
**500** | Internal Server Error - the server could not process the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

