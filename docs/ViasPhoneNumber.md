# ViasPhoneNumber


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phone_country_code** | **str** | The two-character country code of the phone number. &gt;The permitted country codes are defined in ISO-3166-1 alpha-2 (e.g. &#39;NL&#39;). | 
**phone_number** | **str** | The phone number. &gt;The inclusion of the phone number country code is not necessary. | 
**phone_type** | **str** | The type of the phone number. &gt;The following values are permitted: &#x60;Landline&#x60;, &#x60;Mobile&#x60;, &#x60;SIP&#x60;, &#x60;Fax&#x60;. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


