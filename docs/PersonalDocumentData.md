# PersonalDocumentData


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The type of the document. Possible values: **ID**, **DRIVINGLICENSE**, **PASSPORT**, **SOCIALSECURITY**, **VISA**.  To delete an existing entry for a document &#x60;type&#x60;, send only the &#x60;type&#x60; field in your request.  | 
**expiration_date** | **str** | The expiry date of the document,   in ISO-8601 YYYY-MM-DD format. For example, **2000-01-31**. | [optional] 
**issuer_country** | **str** | The country where the document was issued, in the two-character  [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) format. For example, **NL**. | [optional] 
**issuer_state** | **str** | The state where the document was issued (if applicable). | [optional] 
**number** | **str** | The number in the document. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


