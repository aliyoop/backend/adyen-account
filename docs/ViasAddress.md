# ViasAddress


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **str** | The two-character country code of the address. The permitted country codes are defined in ISO-3166-1 alpha-2 (e.g. &#39;NL&#39;). &gt; If you don&#39;t know the country or are not collecting the country from the shopper, provide &#x60;country&#x60; as &#x60;ZZ&#x60;. | 
**city** | **str** | The name of the city. &gt;Required if either &#x60;houseNumberOrName&#x60;, &#x60;street&#x60;, &#x60;postalCode&#x60;, or &#x60;stateOrProvince&#x60; are provided. | [optional] 
**house_number_or_name** | **str** | The number or name of the house. | [optional] 
**postal_code** | **str** | The postal code. &gt;A maximum of five (5) digits for an address in the USA, or a maximum of ten (10) characters for an address in all other countries. &gt;Required if either &#x60;houseNumberOrName&#x60;, &#x60;street&#x60;, &#x60;city&#x60;, or &#x60;stateOrProvince&#x60; are provided. | [optional] 
**state_or_province** | **str** | The abbreviation of the state or province. &gt;Two (2) characters for an address in the USA or Canada, or a maximum of three (3) characters for an address in all other countries. &gt;Required for an address in the USA or Canada if either &#x60;houseNumberOrName&#x60;, &#x60;street&#x60;, &#x60;city&#x60;, or &#x60;postalCode&#x60; are provided. | [optional] 
**street** | **str** | The name of the street. &gt;The house number should not be included in this field; it should be separately provided via &#x60;houseNumberOrName&#x60;. &gt;Required if either &#x60;houseNumberOrName&#x60;, &#x60;city&#x60;, &#x60;postalCode&#x60;, or &#x60;stateOrProvince&#x60; are provided. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


