# UpdateAccountHolderRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_holder_code** | **str** | The code of the Account Holder to be updated. | 
**account_holder_details** | [**AccountHolderDetails**](AccountHolderDetails.md) |  | [optional] 
**description** | **str** | A description of the account holder, maximum 256 characters. You can use alphanumeric characters (A-Z, a-z, 0-9), white spaces, and underscores &#x60;_&#x60;. | [optional] 
**legal_entity** | **str** | The legal entity type of the account holder. This determines the information that should be provided in the request.  Possible values: **Business**, **Individual**, or **NonProfit**.  * If set to **Business** or **NonProfit**, then &#x60;accountHolderDetails.businessDetails&#x60; must be provided, with at least one entry in the &#x60;accountHolderDetails.businessDetails.shareholders&#x60; list.  * If set to **Individual**, then &#x60;accountHolderDetails.individualDetails&#x60; must be provided. | [optional] 
**primary_currency** | **str** | The primary three-character [ISO currency code](https://docs.adyen.com/development-resources/currency-codes), to which the account holder should be updated. | [optional] 
**processing_tier** | **int** | The processing tier to which the Account Holder should be updated. &gt;The processing tier can not be lowered through this request.  &gt;Required if accountHolderDetails are not provided. | [optional] 
**verification_profile** | **str** | The identifier of the profile that applies to this entity. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


