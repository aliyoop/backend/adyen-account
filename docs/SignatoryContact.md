# SignatoryContact


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**ViasAddress**](ViasAddress.md) |  | [optional] 
**email** | **str** | The e-mail address of the person. | [optional] 
**full_phone_number** | **str** | The phone number of the person provided as a single string.  It will be handled as a landline phone. Examples: \&quot;0031 6 11 22 33 44\&quot;, \&quot;+316/1122-3344\&quot;, \&quot;(0031) 611223344\&quot; | [optional] 
**job_title** | **str** | Job title of the signatory.  Example values: **Chief Executive Officer**, **Chief Financial Officer**, **Chief Operating Officer**, **President**, **Vice President**, **Executive President**, **Managing Member**, **Partner**, **Treasurer**, **Director**, or **Other**. | [optional] 
**name** | [**ViasName**](ViasName.md) |  | [optional] 
**personal_data** | [**ViasPersonalData**](ViasPersonalData.md) |  | [optional] 
**phone_number** | [**ViasPhoneNumber**](ViasPhoneNumber.md) |  | [optional] 
**signatory_code** | **str** | The unique identifier (UUID) of the Signatory. &gt;**If, during an Account Holder create or update request, this field is left blank (but other fields provided), a new Signatory will be created with a procedurally-generated UUID.**  &gt;**If, during an Account Holder create request, a UUID is provided, the creation of the Signatory will fail while the creation of the Account Holder will continue.**  &gt;**If, during an Account Holder update request, a UUID that is not correlated with an existing Signatory is provided, the update of the Signatory will fail.**  &gt;**If, during an Account Holder update request, a UUID that is correlated with an existing Signatory is provided, the existing Signatory will be updated.**  | [optional] 
**signatory_reference** | **str** | Your reference for the Signatory. | [optional] 
**web_address** | **str** | The URL of the person&#39;s website. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


