# CloseStoresRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_holder_code** | **str** | The code of the account holder. | 
**stores** | **[str]** | List of stores to be closed. | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


