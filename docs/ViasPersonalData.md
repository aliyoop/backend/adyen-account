# ViasPersonalData


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_of_birth** | **str** | The person&#39;s date of birth, in ISO-8601 YYYY-MM-DD format. For example, **2000-01-31**. | [optional] 
**document_data** | [**[PersonalDocumentData]**](PersonalDocumentData.md) | Array that contains information about the person&#39;s identification document. You can submit only one entry per document type. | [optional] 
**nationality** | **str** | The nationality of the person represented by a two-character country code,  in [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) format. For example, **NL**.  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


