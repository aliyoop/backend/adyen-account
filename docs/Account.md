# Account


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_code** | **str** | The code of the account. | [optional] 
**bank_account_uuid** | **str** | The bankAccountUUID of the bank account held by the account holder to couple the account with. Scheduled payouts in currencies matching the currency of this bank account will be sent to this bank account. Payouts in different currencies will be sent to a matching bank account of the account holder. | [optional] 
**beneficiary_account** | **str** | The beneficiary of the account. | [optional] 
**beneficiary_merchant_reference** | **str** | The reason that a beneficiary has been set up for this account. This may have been supplied during the setup of a beneficiary at the discretion of the executing user. | [optional] 
**description** | **str** | A description of the account. | [optional] 
**metadata** | **{str: (str,)}** | A set of key and value pairs for general use by the merchant. The keys do not have specific names and may be used for storing miscellaneous data as desired. &gt; Note that during an update of metadata, the omission of existing key-value pairs will result in the deletion of those key-value pairs. | [optional] 
**payout_method_code** | **str** | The payout method code held by the account holder to couple the account with. Scheduled card payouts will be sent using this payout method code. | [optional] 
**payout_schedule** | [**PayoutScheduleResponse**](PayoutScheduleResponse.md) |  | [optional] 
**payout_speed** | **str** | Speed with which payouts for this account are processed. Permitted values: &#x60;STANDARD&#x60;, &#x60;SAME_DAY&#x60;. | [optional] 
**status** | **str** | The status of the account. Possible values: &#x60;Active&#x60;, &#x60;Inactive&#x60;, &#x60;Suspended&#x60;, &#x60;Closed&#x60;. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


