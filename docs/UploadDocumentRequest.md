# UploadDocumentRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_content** | **str** | The content of the document, in Base64-encoded string format.  To learn about document requirements, refer to [Verification checks](https://docs.adyen.com/platforms/verification-checks). | 
**document_detail** | [**DocumentDetail**](DocumentDetail.md) |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


