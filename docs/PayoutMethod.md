# PayoutMethod


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merchant_account** | **str** |  | 
**recurring_detail_reference** | **str** |  | 
**shopper_reference** | **str** |  | 
**payout_method_code** | **str** |  | [optional] 
**payout_method_type** | **str** |  | [optional]  if omitted the server will use the default value of "CardToken"
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


