# KYCCheckSummary


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kyc_check_code** | **int** | The code of the check. For possible values, refer to [Verification codes](https://docs.adyen.com/platforms/verification-codes). | [optional] 
**kyc_check_description** | **str** | A description of the check. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


