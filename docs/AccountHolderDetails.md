# AccountHolderDetails


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** | The email address of the account holder. | 
**full_phone_number** | **str** | The phone number of the account holder provided as a single string. It will be handled as a landline phone. **Examples:** \&quot;0031 6 11 22 33 44\&quot;, \&quot;+316/1122-3344\&quot;, \&quot;(0031) 611223344\&quot; | 
**web_address** | **str** | The URL of the website of the account holder. | 
**address** | [**ViasAddress**](ViasAddress.md) |  | [optional] 
**bank_account_details** | [**[BankAccountDetail]**](BankAccountDetail.md) | Array of bank accounts associated with the account holder. For details about the required &#x60;BankAccountDetail&#x60; fields, refer to [Bank account check](https://docs.adyen.com/platforms/verification-checks/bank-account-check). | [optional] 
**bank_aggregator_data_reference** | **str** | The opaque reference value returned by the Adyen API during bank account login. | [optional] 
**business_details** | [**BusinessDetails**](BusinessDetails.md) |  | [optional] 
**individual_details** | [**IndividualDetails**](IndividualDetails.md) |  | [optional] 
**legal_arrangements** | [**[LegalArrangementDetail]**](LegalArrangementDetail.md) | Array that contains information about legal arrangements, used when the account holder is acting on behalf of different parties or is part of a contractual business agreement. | [optional] 
**merchant_category_code** | **str** | The Merchant Category Code of the account holder. &gt; If not specified in the request, this will be derived from the platform account (which is configured by Adyen). | [optional] 
**metadata** | **{str: (str,)}** | A set of key and value pairs for general use by the account holder or merchant. The keys do not have specific names and may be used for storing miscellaneous data as desired. &gt; The values being stored have a maximum length of eighty (80) characters and will be truncated if necessary. &gt; Note that during an update of metadata, the omission of existing key-value pairs will result in the deletion of those key-value pairs. | [optional] 
**payout_methods** | [**[PayoutMethod]**](PayoutMethod.md) | Array of tokenized card details associated with the account holder. For details about how you can use the tokens to pay out, refer to [Pay out to cards](https://docs.adyen.com/platforms/payout-to-cards). | [optional] 
**principal_business_address** | [**ViasAddress**](ViasAddress.md) |  | [optional] 
**store_details** | [**[StoreDetail]**](StoreDetail.md) | Array of stores associated with the account holder. Required when onboarding account holders that have an Adyen [point of sale](https://docs.adyen.com/platforms/platforms-for-pos). | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


