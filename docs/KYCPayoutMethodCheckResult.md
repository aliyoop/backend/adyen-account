# KYCPayoutMethodCheckResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checks** | [**[KYCCheckStatusData]**](KYCCheckStatusData.md) | A list of the checks and their statuses. | [optional] 
**payout_method_code** | **str** | The unique ID of the payoput method to which the check applies. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


