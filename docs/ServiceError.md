# ServiceError


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_code** | **str** | The error code mapped to the error message. | [optional] 
**error_type** | **str** | The category of the error. | [optional] 
**message** | **str** | A short explanation of the issue. | [optional] 
**psp_reference** | **str** | The PSP reference of the payment. | [optional] 
**status** | **int** | The HTTP response status. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


