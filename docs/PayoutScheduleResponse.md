# PayoutScheduleResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schedule** | **str** | The payout schedule of the account. &gt;Permitted values: &#x60;DEFAULT&#x60;, &#x60;HOLD&#x60;, &#x60;DAILY&#x60;, &#x60;WEEKLY&#x60;, &#x60;MONTHLY&#x60;. | 
**next_scheduled_payout** | **datetime** | The date of the next scheduled payout. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


