# LegalArrangementDetail


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**ViasAddress**](ViasAddress.md) |  | [optional] 
**legal_arrangement_code** | **str** | Adyen-generated unique alphanumeric identifier (UUID) for the entry, returned in the response when you create a legal arrangement. Required when updating an existing legal arrangement entry in an &#x60;/updateAccountHolder&#x60; request. | [optional] 
**legal_arrangement_entities** | [**[LegalArrangementEntityDetail]**](LegalArrangementEntityDetail.md) | Array that contains information about the entities or members that are part of the legal arrangement. | [optional] 
**legal_arrangement_reference** | **str** | Your reference for the legal arrangement. Must be between 3 to 128 characters. | [optional] 
**legal_form** | **str** | The structure of the legal arrangement as defined according to legislations in the country.   Possible values: **CashManagementTrust**, **CorporateUnitTrust**, **DeceasedEstate**, **DiscretionaryInvestmentTrust**, **DiscretionaryServicesManagementTrust**, **DiscretionaryTradingTrust**, **FirstHomeSaverAccountsTrust**, **FixedTrust**, **FixedUnitTrust**, **HybridTrust**, **ListedPublicUnitTrust**, **OtherTrust**, **PooledSuperannuationTrust**, **PublicTradingTrust**, **UnlistedPublicUnitTrust**, **LimitedPartnership**, **FamilyPartnership**, **OtherPartnership**. | [optional] 
**name** | **str** | The legal name of the legal arrangement. | [optional] 
**registration_number** | **str** | The registration number of the legal arrangement. | [optional] 
**tax_number** | **str** | The tax identification number of the legal arrangement. | [optional] 
**type** | **str** | The type of legal arrangement.  Possible values:  - **Trust** - A legal agreement where the account holder is a trustee that manages assets for beneficiaries. - **Partnership** - A legal arrangement where the account holder is a partner that has an agreement with one or more partners to manage, operate and share profits of their jointly-owned business. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


