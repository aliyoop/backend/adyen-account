# AccountProcessingState


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disable_reason** | **str** | The reason why processing has been disabled. | [optional] 
**disabled** | **bool** | Indicates whether the processing of payments is allowed. | [optional] 
**processed_from** | [**Amount**](Amount.md) |  | [optional] 
**processed_to** | [**Amount**](Amount.md) |  | [optional] 
**tier_number** | **int** | The processing tier that the account holder occupies. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


