# Amount


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **str** | The three-character [ISO currency code](https://docs.adyen.com/development-resources/currency-codes). | 
**value** | **int** | The amount of the transaction, in [minor units](https://docs.adyen.com/development-resources/currency-codes). | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


