#!/usr/bin/env sh

docker run --rm -v "${PWD}:/out" --user "$(id -u):$(id -g)" \
  openapitools/openapi-generator-cli generate -o /out -c /out/openapi-generator.yaml
